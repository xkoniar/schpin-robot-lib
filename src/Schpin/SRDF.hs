{-# LANGUAGE DuplicateRecordFields #-}

module Schpin.SRDF where

import           Schpin.Shared

data Attribute = Attribute
  { name  :: UrdfName
  , value :: String
  }

data GenericNode = GenericNode
  { name       :: String
  , attributes :: [Attribute]
  , children   :: [GenericNode]
  }

data Chain = Chain
  { base_link :: UrdfName
  , tip_link  :: UrdfName
  }

data Joint = Joint
  { name          :: UrdfName
  , generic_nodes :: [GenericNode]
  }

data JointState = JointState
  { name  :: UrdfName
  , value :: Angle
  }

newtype Link = Link {
            name :: UrdfName
        };


data Group = Group
  { name          :: UrdfName
  , links         :: [Link]
  , joints        :: [Joint]
  , chains        :: [Chain]
  , groups        :: [Group]
  , generic_nodes :: [GenericNode]
  }

data VirtualJointType = Fixed | Floating | Planar

data SRDFItem = EndEffector{
        name         :: UrdfName,
        group        :: UrdfName,
        parent_link  :: UrdfName,
        parent_group :: UrdfName
    } | VirtualJoint {
        name         :: UrdfName,
        child_link   :: UrdfName,
        parent_frame :: UrdfName,
        j_type       :: VirtualJointType
    }| GroupState {
        joints :: [JointState],
        name   :: UrdfName,
        group  :: UrdfName
    } | DisableCollisions {
        link1  :: UrdfName,
        link2  :: UrdfName,
        reason :: String
    };

data SRDF = SRDF
  { name          :: UrdfName
  , groups        :: [Group]
  , items         :: [SRDFItem]
  , generic_nodes :: [GenericNode]
  }
