
module Schpin.Package where

import           Control.Monad.Reader (Reader)
import           Schpin.Shared

data Person = Person
  { per_name  :: String
  , per_email :: String
  }

data PackageVersion = PackageVersion Int Int Int

data Export = BuildType String | ArchitectureIndependent | ManualExport { mexport_tag :: String, mexport_value :: String }

data PackageInfo = PackageInfo
  { pkg_name    :: String
  , version     :: PackageVersion
  , description :: String
  , maintainers :: [Person]
  , license     :: String
  , authors     :: [Person]
  , depends     :: [String]
  , exports     :: [Export]
  }

data PackageConfig = PackageConfig
  { pkg_info :: PackageInfo
  , scad_dir :: Filename
  , pkg_dir  :: Filename
  , src_dir  :: Filename
  }

type PackageMonad a = Reader PackageConfig a

