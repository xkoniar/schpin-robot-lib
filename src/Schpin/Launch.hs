{-# LANGUAGE DuplicateRecordFields #-}

module Schpin.Launch where

import           Schpin.Shared

data Arg = Arg
  { name  :: String
  , def   :: Maybe String
  , value :: Maybe String
  , doc   :: Maybe String
  }

data RosparamCmd = Load | Dump | Delete

data Rosparam = Rosparam
  { command     :: Maybe RosparamCmd
  , file        :: String
  , param       :: String
  , ns          :: Maybe String
  , subst_value :: Maybe Bool
  , content     :: String
  }

data ParamType = Str | Int | Double | Bool | Yaml

data Param = Param
  { name     :: String
  , value    :: Maybe String
  , ptype    :: Maybe ParamType
  , textfile :: Maybe Filename
  , binfile  :: Maybe Filename
  , command  :: Maybe String
  }

defParam :: Param
defParam = Param { name = ""
                 , value = Nothing
                 , ptype = Nothing
                 , textfile = Nothing
                 , binfile = Nothing
                 , command = Nothing
  }

data Env = Env
  { name  :: String
  , value :: String
  }

data Remap = Remap
  { from :: String
  , to   :: String
  }

data Include = Include
  { file          :: String
  , ns            :: Maybe String
  , clear_params  :: Maybe Bool
  , pass_all_args :: Maybe Bool
  , envs          :: [Env]
  , args          :: [Arg]
  }

data Machine = Machine
  { name       :: String
  , address    :: String
  , env_loader :: Filename
  , def        :: Maybe Bool
  , user       :: Maybe String
  , password   :: Maybe String
  , timeout    :: Maybe Float
  , envs       :: [Env]
  }

data Node = Node
  { pkg           :: String
  , ntype         :: String
  , name          :: String
  , args          :: Maybe String
  , machine       :: Maybe String
  , respawn       :: Maybe Bool
  , respawn_delay :: Maybe Int
  , required      :: Maybe Bool
  , ns            :: Maybe String
  , clear_params  :: Maybe Bool
  , output        :: Maybe Bool
  , cwd           :: Maybe String
  , launch_prefix :: Maybe String
  , envs          :: [Env]
  , remaps        :: [Remap]
  , rosparams     :: [Rosparam]
  , params        :: [Param]
  }

data Test = Test
  { pkg           :: String
  , test_name     :: String
  , ntype         :: String
  , nname         :: Maybe String
  , args          :: Maybe String
  , clear_params  :: Maybe Bool
  , cwd           :: Maybe String
  , launch_prefix :: Maybe String
  , ns            :: Maybe String
  , retry         :: Maybe Int
  , time_limit    :: Maybe Float
  , envs          :: [Env]
  , remaps        :: [Remap]
  , rosparams     :: [Rosparam]
  , params        :: [Param]
  }

data Group = Group
  { ns           :: Maybe String
  , clear_params :: Maybe Bool
  , nodes        :: [Node]
  , params       :: [Param]
  , remaps       :: [Remap]
  , machines     :: [Machine]
  , rosparams    :: [Rosparam]
  , includes     :: [Include]
  , envs         :: [Env]
  , tests        :: [Test]
  , args         :: [Arg]
  }

data Launch = Launch
  { deprecated :: Maybe String
  , nodes      :: [Node]
  , params     :: [Param]
  , remaps     :: [Remap]
  , machines   :: [Machine]
  , rosparams  :: [Rosparam]
  , includes   :: [Include]
  , envs       :: [Env]
  , tests      :: [Test]
  , args       :: [Arg]
  , groups     :: [Group]
  }

defLaunch :: Launch
defLaunch = Launch { deprecated = Nothing,
  nodes = [],
  params = [],
  remaps = [],
  machines = [],
  rosparams = [],
  includes = [],
  envs = [],
  tests = [],
  args = [],
  groups = []
                   }
