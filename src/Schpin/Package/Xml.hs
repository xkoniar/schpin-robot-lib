
module Schpin.Package.Xml where

import           Schpin.Package
import           Schpin.Shared
import           Text.XML.HXT.Core


cVersionToXML :: ArrowXml a => PackageVersion -> a XmlTree XmlTree
cVersionToXML (PackageVersion x y z) = selem "version" [ txt $ strver ]
  where
    strver = show x ++ "." ++ show y ++ "." ++ show z

cMaintainerToXML :: ArrowXml a => Person -> a XmlTree XmlTree
cMaintainerToXML (Person name mail) = mkelem "maintainer" [sattr "email" mail] [txt name]

cAuthorToXML :: ArrowXml a => Person -> a XmlTree XmlTree
cAuthorToXML (Person name mail) = mkelem "author" [sattr "email" mail] [txt name]

cExportToXML :: ArrowXml a => Export -> a XmlTree XmlTree
cExportToXML (BuildType name)          = selem "build_type" [ txt $ name ]
cExportToXML (ArchitectureIndependent) = selem "architecture_independent" []
cExportToXML (ManualExport tag value)  = selem tag [ txt $ value ]

cPackageToXML :: ArrowXml a => PackageInfo -> a XmlTree XmlTree
cPackageToXML (PackageInfo name ver desc maints lice authors deps exprts) = mkelem "package" [sattr "format" "2"] $
  [ selem "name" [ txt name ]
  , cVersionToXML ver
  , selem "description" [ txt desc ]
  , selem "license" [ txt lice ]
  , mkelem "export" [] (cExportToXML <$> exprts)
  ]
   ++ (cMaintainerToXML <$> maints)
   ++ (cAuthorToXML <$> authors)
   ++ ((\x -> selem "depend" [txt x]) <$> deps)

writePackageXML :: PackageConfig -> IO()
writePackageXML pcfg = do
   let filename = pkg_dir pcfg ++ "/package.xml"
   makeParentDirectories filename
   runX $ Text.XML.HXT.Core.root [] [cPackageToXML (pkg_info pcfg)] >>> writeDocument [withIndent yes] filename
   return ()
