module Schpin.Launch.Xml where

import           Schpin.Launch
import           Schpin.Shared
import           Text.XML.HXT.Core

cArgToXML :: ArrowXml a => Arg -> a XmlTree XmlTree
cArgToXML (Arg name mb_def mb_val mb_doc) = aelem
  "arg"
  (  [sattr "name" name]
  ++ (optionalAttrToList "default" mb_def)
  ++ (optionalAttrToList "value" mb_val)
  ++ (optionalAttrToList "doc" mb_doc)
  )

paramCmdToString :: RosparamCmd -> String
paramCmdToString cmd = case cmd of
  Load   -> "load"
  Dump   -> "dump"
  Delete -> "delete"

cRosparamToXML :: ArrowXml a => Rosparam -> a XmlTree XmlTree
cRosparamToXML (Rosparam mb_cmd file param mb_ns mb_subst_value content) =
  mkelem
    "rosparam"
    (  [sattr "file" file, sattr "param" param]
    ++ (optionalAttrToList "command" $ paramCmdToString <$> mb_cmd)
    ++ (optionalAttrToList "ns" mb_ns)
    ++ (optionalAttrToList "subst_value" mb_subst_value)
    )
    [txt content]

paramTypeToString :: ParamType -> String
paramTypeToString par = case par of
  Str    -> "str"
  Int    -> "int"
  Double -> "double"
  Bool   -> "bool"
  Yaml   -> "yaml"

cParamToXML :: ArrowXml a => Param -> a XmlTree XmlTree
cParamToXML (Param name mb_value mb_ptype mb_textfile mb_binfile mb_cmd) =
  aelem
    "param"
    (  [sattr "name" name]
    ++ (optionalAttrToList "value" mb_value)
    ++ (optionalAttrToList "type" $ paramTypeToString <$> mb_ptype)
    ++ (optionalAttrToList "textfile" mb_textfile)
    ++ (optionalAttrToList "binfile" mb_binfile)
    ++ (optionalAttrToList "command" mb_cmd)
    )

cEnvToXML :: ArrowXml a => Env -> a XmlTree XmlTree
cEnvToXML (Env name value) =
  aelem "env" [sattr "name" name, sattr "value" value]

cRemapToXML :: ArrowXml a => Remap -> a XmlTree XmlTree
cRemapToXML (Remap from to) = aelem "remap" [sattr "from" from, sattr "to" to]

cIncludeToXML :: ArrowXml a => Include -> a XmlTree XmlTree
cIncludeToXML (Include file mb_ns mb_clear_params mb_pass_all_args envs args) =
  mkelem
    "include"
    (  [sattr "file" file]
    ++ (optionalAttrToList "ns" mb_ns)
    ++ (optionalAttrToList "clear_params" mb_clear_params)
    ++ (optionalAttrToList "pass_all_args" mb_pass_all_args)
    )
    ((cEnvToXML <$> envs) ++ (cArgToXML <$> args))


cMachineToXML :: ArrowXml a => Machine -> a XmlTree XmlTree
cMachineToXML (Machine name addr env_loader mb_def mb_user mb_pass mb_timeout envs)
  = mkelem
    "machine"
    (  [sattr "name" name, sattr "address" addr, sattr "env-loader" env_loader]
    ++ (optionalAttrToList "default" mb_def)
    ++ (optionalAttrToList "user" mb_user)
    ++ (optionalAttrToList "password" mb_pass)
    ++ (optionalAttrToList "timeout" mb_timeout)
    )
    (cEnvToXML <$> envs)

cNodeToXML :: ArrowXml a => Node -> a XmlTree XmlTree
cNodeToXML (Node pkg ntype name mb_args mb_machine mb_respawn mb_respawn_delay mb_required mb_ns mb_clear_params mb_output mb_cwd mb_launch_prefix envs remaps rosparams params)
  = mkelem
    "node"
    (  [sattr "pkg" pkg, sattr "type" ntype, sattr "name" name]
    ++ (optionalAttrToList "args" mb_args)
    ++ (optionalAttrToList "machine" mb_machine)
    ++ (optionalAttrToList "respawn" mb_respawn)
    ++ (optionalAttrToList "respawn_delay" mb_respawn_delay)
    ++ (optionalAttrToList "required" mb_required)
    ++ (optionalAttrToList "ns" mb_ns)
    ++ (optionalAttrToList "clear_params" mb_clear_params)
    ++ (optionalAttrToList "cwd" mb_cwd)
    ++ (optionalAttrToList "launch_prefix" mb_launch_prefix)
    )
    (  (cParamToXML <$> params)
    ++ (cRemapToXML <$> remaps)
    ++ (cRosparamToXML <$> rosparams)
    ++ (cEnvToXML <$> envs)
    )

cTestToXML :: ArrowXml a => Test -> a XmlTree XmlTree
cTestToXML (Test pkg test_name ntype mb_nname mb_args mb_clear_params mb_cwd mb_launch_prefix mb_ns mb_retry mb_time_limit envs remaps rosparams params)
  = mkelem
    "test"
    (  [sattr "pkg" pkg, sattr "test_name" test_name, sattr "type" ntype]
    ++ (optionalAttrToList "name" mb_nname)
    ++ (optionalAttrToList "args" mb_args)
    ++ (optionalAttrToList "clear_params" mb_clear_params)
    ++ (optionalAttrToList "cwd" mb_cwd)
    ++ (optionalAttrToList "launch_prefix" mb_launch_prefix)
    ++ (optionalAttrToList "ns" mb_ns)
    ++ (optionalAttrToList "retry" mb_retry)
    ++ (optionalAttrToList "time_limit" mb_time_limit)
    )
    (  (cParamToXML <$> params)
    ++ (cRemapToXML <$> remaps)
    ++ (cRosparamToXML <$> rosparams)
    ++ (cEnvToXML <$> envs)
    )

cGroupToXML :: ArrowXml a => Group -> a XmlTree XmlTree
cGroupToXML (Group mb_ns mb_clear_params nodes params remaps machines rosparams includes envs tests args)
  = mkelem
    "group"
    (  (optionalAttrToList "ns" mb_ns)
    ++ (optionalAttrToList "clear_params" mb_clear_params)
    )
    (  (cNodeToXML <$> nodes)
    ++ (cParamToXML <$> params)
    ++ (cRemapToXML <$> remaps)
    ++ (cMachineToXML <$> machines)
    ++ (cRosparamToXML <$> rosparams)
    ++ (cIncludeToXML <$> includes)
    ++ (cEnvToXML <$> envs)
    ++ (cTestToXML <$> tests)
    ++ (cArgToXML <$> args)
    )

cLaunchToXML :: ArrowXml a => Launch -> a XmlTree XmlTree
cLaunchToXML (Launch mb_deprecated nodes params remaps machines rosparams includes envs tests args groups)
  = mkelem
    "launch"
    (optionalAttrToList "deprecated" mb_deprecated)
    (  (cNodeToXML <$> nodes)
    ++ (cParamToXML <$> params)
    ++ (cRemapToXML <$> remaps)
    ++ (cMachineToXML <$> machines)
    ++ (cRosparamToXML <$> rosparams)
    ++ (cIncludeToXML <$> includes)
    ++ (cEnvToXML <$> envs)
    ++ (cTestToXML <$> tests)
    ++ (cArgToXML <$> args)
    ++ (cGroupToXML <$> groups)
    )

writeLaunchXML :: Launch -> Filename -> IO ()
writeLaunchXML lnch filename = do
  makeParentDirectories filename
  runX
    $   Text.XML.HXT.Core.root [] [cLaunchToXML lnch]
    >>> writeDocument [withIndent yes] filename
  return ()
