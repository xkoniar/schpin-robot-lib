{-# LANGUAGE NamedFieldPuns #-}

module Schpin.Walker.Config where

import           Data.ByteString.Lazy.UTF8         (toString)
import           Data.Maybe                        (catMaybes)
import           Data.Scientific
import           Data.Foldable                     (toList)
import           Data.Text                         (pack)
import           Data.YAML
import           Numeric.Units.Dimensional.Prelude (radian, (/~), meter)
import           Schpin.Package
import           Schpin.Shared
import           Schpin.Walker

acTypeToString :: ActuatorType -> String
acTypeToString t = case t of
  Lewan {}     -> "LEWAN"
  LewanTest {} -> "LEWAN_TEST"
  Dynamixel {} -> "DYNMX"

writeControlConfig :: ExtendedWalker -> PackageConfig -> IO ()
writeControlConfig walker cfg = do
  makeParentDirectories filename
  writeFile filename $ toString $ encode1 out
 where
  filename :: FilePath
  filename = pkg_dir cfg ++ "/config/control.yml"

  out :: Node ()
  out =
    mapping [pack "control_node" .= mapping [pack "ros__parameters" .= params]]

  freq :: Double
  freq = 1.0

  params :: Node ()
  params = mapping
    [ pack "joint_state" .= pack "/control/joint_states"
    , pack "diag_freq" .= freq
    , pack "diag_topic" .= pack "/diagnostics"
    , pack "legs" .= legsp
    , pack "servos" .= servos
    ]

  legsp :: Node ()
  legsp = mapping $ (\l -> (pack ("leg/" ++ leg_name l)) .= cfg l) <$>(legs walker)
    where
      cfg :: ExtendedLeg -> Node()
      cfg Leg{root_joint, neutral_dir} = mapping $
        [ pack "neutral_angles" .= ((\x -> x /~ radian) <$> neutral_angles root_joint)
        , pack "neutral_dir" .= ((\x -> x /~ meter) <$> toList neutral_dir)
        ]

      neutral_angles :: ExtendedJoint -> [Angle]
      neutral_angles root_joint = catMaybes $ mapJoints f root_joint
        where
          f :: ExtendedJoint -> Maybe Angle
          f ExtendedJoint{joint=Joint{jtype}} = case jtype of
              RevoluteJoint{default_angle} -> Just $ default_angle
              _ -> Nothing


  servos :: Node ()
  servos =
    mapping
      $   catMaybes
      $   concat
      $   (\Leg { root_joint } -> mapJoints joint_to_conf root_joint)
      <$> (legs walker)

  joint_to_conf :: ExtendedJoint -> Maybe Pair
  joint_to_conf (ExtendedJoint (Joint { joint_name, jtype }) uname) = do
    conf <- cfg jtype
    return ((pack uname) .= conf)
   where

    cfg :: JointType -> Maybe (Node ())
    cfg RevoluteJoint { actuator, min_angle, max_angle } = Just $ mapping $
      [ pack "id" .= ac_id actuator
      , pack "type" .= pack (acTypeToString atype)
      , pack "joint_name" .= pack uname
      , pack "from_servo" .= ac_from actuator
      , pack "to_servo" .= ac_to actuator
      , pack "from_angle" .= (min_angle /~ radian)
      , pack "to_angle" .= (max_angle /~ radian)
      , pack "stall_range" .= (5 :: Int)
      , pack "angle_change" .= (5 :: Int)
      ] ++ case atype of
             Lewan {} -> catMaybes  [
                 opt_arg "goto_p" $ lw_goto_p atype
                , opt_arg "goto_i" $ lw_goto_i atype
                , opt_arg "goto_d" $ lw_goto_d atype
                , opt_arg "vel_p" $ lw_vel_p atype
                , opt_arg "vel_i" $ lw_vel_i atype
                , opt_arg "vel_d" $ lw_vel_d atype
                      ]
             LewanTest {} -> catMaybes  [
                 opt_arg "goto_p" $ lwt_goto_p atype
                , opt_arg "goto_i" $ lwt_goto_i atype
                , opt_arg "goto_d" $ lwt_goto_d atype
                , opt_arg "vel_p" $ lwt_vel_p atype
                , opt_arg "vel_i" $ lwt_vel_i atype
                , opt_arg "vel_d" $ lwt_vel_d atype
                      ]
             Dynamixel {} -> catMaybes
              [ opt_arg "goto_p" $ dn_goto_p atype
              , opt_arg "goto_i" $ dn_goto_i atype
              , opt_arg "goto_d" $ dn_goto_d atype
              , opt_arg "vel_p" $ dn_vel_p atype
              , opt_arg "vel_i" $ dn_vel_i atype
              ]
      where
        atype = ac_type actuator

        opt_arg :: ToYAML a => String -> Maybe a -> Maybe Pair
        opt_arg k Nothing = Nothing
        opt_arg k (Just s) = Just (pack k .= s)

    cfg FixedJoint = Nothing

