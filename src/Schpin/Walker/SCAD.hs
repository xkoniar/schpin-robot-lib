{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE NamedFieldPuns        #-}

module Schpin.Walker.SCAD where

import           Data.List.Unique  (sortUniq)
import           Language.OpenSCAD
import           Schpin.SCAD
import           Schpin.Shared
import           Schpin.Walker

poseToSCAD :: Pose -> [Object] -> Object
poseToSCAD pose children =
  translate (pos pose) $ qrotate (ori pose) $ union children


extendedModelToSCAD :: ExtendedSCADModel -> [TopLevel]
extendedModelToSCAD ExtendedSCADModel { absolute_imports = imports, smodel = SCADModel { module_name, args } }
  = (UseDirective <$> imports)
    ++ [TopLevelScope $ Module (Ident module_name) (args) Nothing]

getSCADFiles :: ExtendedWalker -> [(Filename, [TopLevel])]
getSCADFiles = mapModels (\x -> (scad_filename x, extendedModelToSCAD x))

getSCADSkeleton :: ExtendedWalker -> (ExtendedAssembly -> Object) -> [TopLevel]
getSCADSkeleton walker ass_to_skeleton =
  (UseDirective <$> imports)
    ++ [ TopLevelScope $ union $ concat (leg_skeleton <$> legs walker)
       , TopLevelScope $ ass_to_skeleton $ body_ass $ base_body $ body walker
       ]
 where
  imports = sortUniq $ concat $ mapModels absolute_imports walker

  leg_skeleton :: ExtendedLeg -> [Object]
  leg_skeleton Leg { root_joint } = foldJoints fold_joint [] root_joint

  fold_joint :: ExtendedJoint -> [Object] -> [Object]
  fold_joint ExtendedJoint { joint } sub =
    [poseToSCAD (offset joint) (content $ link $ sub_link joint)]
   where
    map_ass :: Maybe ExtendedAssembly -> [Object]
    map_ass Nothing       = []
    map_ass (Just assemb) = [ass_to_skeleton assemb]

    content :: Link ExtendedAssembly -> [Object]
    content lin = case jtype joint of
      RevoluteJoint { axis, default_angle } ->
        [rotate axis default_angle $ union $ ((map_ass $ assembly lin) ++ sub)]
      FixedJoint -> (map_ass $ assembly lin) ++ sub

assemblyToVisual :: ExtendedAssembly -> Object
assemblyToVisual Assembly { amodel } = modelToVisual amodel

assemblyToCollision :: ExtendedAssembly -> Object
assemblyToCollision Assembly { collision_models } =
  union
    $   (\ExtendedSCADModel { smodel = SCADModel { module_name, args } } ->
          Module (Ident module_name) args Nothing
        )
    <$> collision_models

modelToVisual :: ExtendedSCADModel -> Object
modelToVisual ExtendedSCADModel { smodel = SCADModel { module_name, args } } =
  Module (Ident module_name) args Nothing


getSCADVisualSkeleton :: ExtendedWalker -> [TopLevel]
getSCADVisualSkeleton walkie = getSCADSkeleton walkie assemblyToVisual

getSCADCollisionSkeleton :: ExtendedWalker -> [TopLevel]
getSCADCollisionSkeleton walkie = getSCADSkeleton walkie assemblyToCollision


getSTLRenderList :: ExtendedWalker -> [(Filename, Filename)]
getSTLRenderList = mapModels model_to_render
 where
  model_to_render :: ExtendedSCADModel -> (Filename, Filename)
  model_to_render ExtendedSCADModel { scad_filename, stl_filename } =
    (scad_filename, stl_filename)

renderWalkerStl :: ExtendedWalker -> IO ()
renderWalkerStl walker = renderSCADFiles $ getSTLRenderList walker
