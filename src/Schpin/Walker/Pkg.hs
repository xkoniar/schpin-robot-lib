{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE NamedFieldPuns        #-}

module Schpin.Walker.Pkg where

import           Control.Monad.Reader (runReader)
import           Schpin.Launch
import           Schpin.Launch.Xml    (writeLaunchXML)
import           Schpin.Package       (PackageConfig, pkg_dir, pkg_info,
                                       pkg_name)
import           Schpin.SCAD          (renderSCAD)
import           Schpin.SRDF.Xml      (writeSRDFXML)
import           Schpin.URDF.Xml      (writeURDFXML)
import           Schpin.Walker        (SimpleWalker, extendWalker, walker_name)
import           Schpin.Walker.SCAD   (getSCADFiles, renderWalkerStl)
import           Schpin.Walker.SRDF   (getSRDF)
import           Schpin.Walker.URDF   (getURDF)



writeCMakeLists :: PackageConfig -> IO ()
writeCMakeLists cfg = writeFile (pkg_dir cfg ++ "/CMakeLists.txt") content
 where
  content :: String
  content = unlines
    [ "cmake_minimum_required(VERSION 3.5)"
    , "project(" ++ pkg_name (pkg_info cfg) ++ ")"
    , "find_package(ament_cmake REQUIRED)"
    , "install(DIRECTORY urdf/ DESTINATION share/${PROJECT_NAME}/urdf/)"
    , "install(DIRECTORY srdf/ DESTINATION share/${PROJECT_NAME}/srdf/)"
    , "install(DIRECTORY stl/ DESTINATION share/${PROJECT_NAME}/stl/)"
    , "install(DIRECTORY config/ DESTINATION share/${PROJECT_NAME}/config/)"
    , "ament_package()"
    ]


generateWalkerPackage :: SimpleWalker -> PackageConfig -> IO ()
generateWalkerPackage walker pc = do
  let extended_walker = extendWalker walker pc
  mapM_ (uncurry renderSCAD) $ getSCADFiles extended_walker
  writeURDFXML
    (getURDF extended_walker)
    (  pkg_dir pc
    ++ "/urdf/robot.urdf"
    )
  writeSRDFXML
    (getSRDF extended_walker)
    (  pkg_dir pc
    ++ "/srdf/robot.srdf"
    )
  renderWalkerStl extended_walker
