{-# LANGUAGE DataKinds            #-}
{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE NamedFieldPuns        #-}

module Schpin.Shared where

import           Data.Maybe                        (maybeToList)
import           Data.Hashable
import           Data.String.ToString
import           Filesystem.Path.CurrentOS         (decodeString, directory,
                                                    encodeString)
import           Linear.Conjugate
import           Linear.Quaternion                 (Quaternion (..), axisAngle,
                                                    rotate)
import           Linear.V3                         (V3 (..))
import           Numeric.NumType.DK.Integers       (TypeInt (Neg1, Neg2, Neg3, Pos1, Pos2, Pos3, Pos4, Zero))
import           Numeric.Units.Dimensional         (Dimension (Dim))
import           Numeric.Units.Dimensional.Prelude (degree, (*~), (/~))
import qualified Numeric.Units.Dimensional.Prelude as U
import           System.Directory                  (createDirectoryIfMissing,
                                                    doesFileExist)
import qualified System.IO.Strict                  as IOS
import           Text.XML.HXT.Core                 (ArrowXml, XmlTree, sattr)

type Effort = Double

type Velocity = U.Velocity Double
type AngularVelocity = U.AngularVelocity Double
type Angle = U.Angle Double
type Length = U.Length Double
type Distance = U.Length Double
type Radius = U.Length Double
type Mass = U.Mass Double
type Friction = U.Force Double
type Inertia = U.MomentOfInertia Double

type DRevoluteFriction = 'Dim 'Pos1 'Pos1 'Neg1 'Zero 'Zero 'Zero 'Zero
type RevoluteFriction = U.Quantity DRevoluteFriction Double

type DDamping = 'Dim 'Zero 'Pos1 'Neg1 'Zero 'Zero 'Zero 'Zero
type Damping = U.Quantity DDamping Double

type DRevoluteDamping = 'Dim 'Pos2 'Pos1 'Neg2 'Zero 'Zero 'Zero 'Zero
type RevoluteDamping = U.Quantity DRevoluteDamping Double

type Multiplier = Double
type Offset = Double
type ColorChannel = Double
type Scale = V3 Double
type UrdfName = String
type Filename = String

type Position = V3 Length

type Axis = Position

mm :: U.Unit U.NonMetric U.DLength Double
mm = U.milli U.metre

zAxis :: Axis
zAxis = V3 (0 *~ mm) (0 *~ mm) (1000 *~ mm)

yAxis :: Axis
yAxis = V3 (0 *~ mm) (1000 *~ mm) (0 *~ mm)

xAxis :: Axis
xAxis = V3 (1000 *~ mm) (0 *~ mm) (0 *~ mm)

toQuat :: Axis -> Angle -> Quaternion Double
toQuat ax angle = axisAngle (posToDouble ax) cangle
 where
  cangle = angle /~ U.radian

neutralQuat :: Quaternion Double
neutralQuat = Quaternion 1 (V3 0 0 0)

neutralPos :: Position
neutralPos = V3 (0 *~ mm) (0 *~ mm) (0 *~ mm)

noOffset :: Pose
noOffset = Pose neutralPos neutralQuat

data Pose = Pose
  { pos :: Position
  , ori :: Quaternion Double
  }
  deriving (Show, Eq, Ord)

instance Hashable Length where
  hashWithSalt s l = s `hashWithSalt` (l /~ mm)

instance Hashable Pose where
  hashWithSalt s Pose{pos, ori} = s `hashWithSalt` pos `hashWithSalt` ori

posToDouble :: Position -> V3 Double
posToDouble (V3 x y z) = V3 (x /~ mm) (y /~ mm) (z /~ mm)

doubleVecToPos :: V3 Double -> Position
doubleVecToPos (V3 x y z) = V3 (x *~ mm) (y *~ mm) (z *~ mm)

rotateQuat :: (Conjugate a, RealFloat a) => Quaternion a -> Quaternion a -> Quaternion a
rotateQuat a b = b * a 

-- transform A B rotates A by B. 
transform :: Pose -> Pose -> Pose
transform (Pose fpos fori) (Pose bpos bori) = Pose { pos = doubleVecToPos $ rotate bori (posToDouble fpos) + (posToDouble bpos),
                                                     ori = rotateQuat fori bori }

eulerToQuat :: V3 Double -> Quaternion Double
eulerToQuat (V3 x y z) = rotateQuat (rotateQuat x_quat y_quat) z_quat
  where
    x_quat = toQuat xAxis (x *~ degree)
    y_quat = toQuat yAxis (y *~ degree)
    z_quat = toQuat zAxis (z *~ degree)

quatToRPY :: Quaternion Double -> V3 Double
quatToRPY (Quaternion w (V3 x y z)) = V3 roll pitch yaw
 where
  clamp :: Double -> Double -> Double -> Double
  clamp min val max | min > val = min
                    | max < val = max
                    | otherwise = val
  t0    = 2 * (w * x + y * z)
  t1    = 1 - 2 * (x * x + y * y)
  roll  = atan2 t0 t1
  t2    = clamp (-1) (2 * (w * y - z * x)) 1
  pitch = asin t2
  t3    = 2 * (w * z + x * y)
  t4    = 1 - 2 * (y * y + z * z)
  yaw   = atan2 t3 t4

makeParentDirectories :: Filename -> IO()
makeParentDirectories fname = createDirectoryIfMissing True $ encodeString $ directory $ decodeString fname

writeFileIfChanged :: Filename -> String -> IO ()
writeFileIfChanged fname content = do
  fileExists      <- doesFileExist fname
  present_content <- if fileExists then IOS.readFile fname else return ""
  if present_content == content then return () else writeFile fname content

instance {-# OVERLAPPABLE #-} Show a => ToString a where
  toString = show

optionalAttrToList
  :: (ArrowXml a, ToString b) => String -> Maybe b -> [a n XmlTree]
optionalAttrToList name maybe = map (sattr name . toString) (maybeToList maybe)
